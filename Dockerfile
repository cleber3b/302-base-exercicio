FROM openjdk:8-alpine
WORKDIR /APP-INVEST
COPY target/Api-Investimentos-*.jar api-invest.jar
CMD ["java", "-jar", "api-invest.jar"]
