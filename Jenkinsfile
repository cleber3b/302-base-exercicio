pipeline {
    agent any

    parameters{
        string(name: "VM_USUARIO", defaultValue: "ubuntu", description: "Nome de usuario da VM de acesso a AWS")
        string(name: "VM_SERVER_IP", defaultValue: "3.16.152.142", description: "IP do Servidor da VM para acesso a AWS")
        string(name: "DOCKER_REGISTRY_URL", defaultValue: "registry-itau.mastertech.com.br", description: "Docker URL Registro Imagens")
        string(name: "DOCKER_MINHA_IMAGEM", defaultValue: "${DOCKER_REGISTRY_URL}/imagem_cleberson", description: "Imagen API Investimento")
    }

    post {
        always {
            cleanWs()
        }
        failure {
            updateGitlabCommitStatus name: "build", state: "failed"
        }
        success {
            updateGitlabCommitStatus name: "build", state: "success"
        }
    }
    options {
        gitLabConnection("gitlab")
    }

    stages{
        stage("Testar"){
            steps{
                echo "*** Executando Testes ***"
                sh "./mvnw test"
                echo "*** Finalizado com Sucesso ***"
            }
        }
        stage("Gerar Pacote") {
            steps {
                echo "*** Gerando pacote (.JAR) ***"
                sh "./mvnw package -DskipTests"
                echo "*** Finalizado com sucesso! ***"
            }
        }

        /* stage("Publicar BugFix"){
            when{branch "bugfix"}
            stages{
                stage("Copiar pacote"){
                    steps{
                        echo "*** Copiando pacote (.JAR) ***"
                        script{
                            def targetPath = sh (script: "find target/ -name 'Api-Investimentos-*.jar'", returnStdout: true).trim()
                            sh "scp -o StrictHostKeyChecking=no ${targetPath} ${params.VM_USUARIO}@${params.VM_SERVER_IP}:/home/ubuntu/"
                        }
                        echo "*** Finalizado com sucesso! ***"
                    }
                }

                stage("Reiniciar Serviço"){
                    steps{
                        echo "*** Reiniciando Microserviço de Investimentos ***"
                        sh "ssh -o StrictHostKeyChecking=no -t ${params.VM_USUARIO}@${params.VM_SERVER_IP} 'sudo systemctl restart apiInvestimento.service'"
                        echo "*** Finalizado com sucesso! ***"
                    }
                }

                stage("Validar Status Microserviço") {
                    steps {
                        script {
                            def statusCode = sh (
                                script: "curl -s -o /dev/null -w '%{http_code}'-X GET 'http://${params.VM_SERVER_IP}/investimentos'",
                                returnStdout: true
                            ).trim()
                            echo "${statusCode}"
                        }
                    }
                }
            }
        } */

        stage("Publicar Master"){
            when{branch "master"}
            /*
            Todos os blocos de stage (Package, Build and push image e Deploy serão executados apenas
            quando o evento disparado pelo SCM for na branch master.
            */
            stages{
                stage("Compilar e Gerar Imagem para Docker"){
                    steps{
                        script {
                            docker.withRegistry("https://${params.DOCKER_REGISTRY_URL}","registry_credential"){
                                def customImage=docker.build("${params.DOCKER_MINHA_IMAGEM}")
                                /* Os dois 'push' abaixo fazem o push da imagem para o registry com a tag latest e a tag do numero do build da pipeline */
                                customImage.push("${env.BUILD_ID}")
                                customImage.push("latest")
                                echo "*** Imagem ${env.BUILD_ID} - ${env.BUILD_TAG} gerada com sucesso!***"
                            }
                        }
                    }
                }
                stage("Deploy"){
                    steps{
                        echo "*** Iniciando Rollout Deployment ***"
                        sh "kubectl rollout restart -n cleberson deployment/clebinho-deployment-invest"
                        echo "*** Finalizado Rollout Deployment com sucesso! ***"

                        /* Como estamos usando Kubernetes somente iremos dar o PULL na imagem no Docker Registry...*/
                        /* echo "*** Iniciando PULL da (${params.DOCKER_MINHA_IMAGEM}:latest) ***"
                        sh "ssh -o StrictHostKeyChecking=no -t ${params.VM_USUARIO}@${params.VM_SERVER_IP} 'docker pull ${params.DOCKER_MINHA_IMAGEM}:latest'"
                        echo "*** Finalizado o PULL da (${params.DOCKER_MINHA_IMAGEM}:latest) ***"

                        echo "*** Iniciando STOP do Container(api_invest_cleberson) ***"
                        sh "ssh -o StrictHostKeyChecking=no -t ${params.VM_USUARIO}@${params.VM_SERVER_IP} 'docker stop api_invest_cleberson'"
                        echo "*** Finalizado STOP do Container(api_invest_cleberson) ***"

                        echo "*** Iniciando RUN na porta 6060 para o Container(api_invest_cleberson) ***"
                        sh "ssh -o StrictHostKeyChecking=no -t ${params.VM_USUARIO}@${params.VM_SERVER_IP} 'docker run --rm -p 6060:8080 -d --name api_invest_cleberson ${params.DOCKER_MINHA_IMAGEM}:latest'"
                        echo "*** Finalizado RUN na porta 6060 para o Container(api_invest_cleberson) ***"

                        echo "*** Apagando Imgens anteriores que ficam com Tag <none> após o pull ***"
                        sh "ssh -o StrictHostKeyChecking=no -t ${params.VM_USUARIO}@${params.VM_SERVER_IP} 'docker system prune -fa --volumes'"
                        echo "*** Finalizando Imgens anteriores que ficam com Tag <none> após o pull ***" */

                    }
                }
            }
        }
    }
}
