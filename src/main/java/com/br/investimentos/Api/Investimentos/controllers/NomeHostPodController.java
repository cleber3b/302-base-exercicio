package com.br.investimentos.Api.Investimentos.controllers;

import com.br.investimentos.Api.Investimentos.services.NomeHostPodContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/hello-world")
public class NomeHostPodController {

    @Autowired
    private NomeHostPodContainer service;

    @GetMapping
    public String helloWorld() {
        try {
            return "Hello World " + service.retrieveInstanceInfo();
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }



    }
}
